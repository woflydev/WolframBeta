# Wolfram | Beta

A Node.js package that uses [Puppeteer](https://pptr.dev) to grab step-by-step solutions from [Wolfram|Alpha®](https://www.wolframalpha.com) queries directly in your terminal (with HTML support) without needing an API key. **Wolfram|Beta** provides a command-line interface to input queries, fetch and parse data from Wolfram|Alpha®, and is fully hackable for future adaptation!

Jump to [Usage](#usage).

![photo1](docs/photo1.png)

## Intent

This project aims to merely serve as an educational demonstration of extending the capabilities of an existing service to the terminal environment.

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [How It Works](#how-it-works)
- [Contributing](#contributing)
- [License](#license)

## Installation

### From Command Line (Recommended for Linux / Mac)

Simply run one of the following commands in your terminal:
```bash
curl -o- https://codeberg.org/woflydev/WolframBeta/raw/branch/main/install.sh | bash
```
```bash
wget -qO- https://codeberg.org/woflydev/WolframBeta/raw/branch/main/install.sh | bash
```

This downloads a script (which you can view the source code of [here](https://codeberg.org/woflydev/WolframBeta/raw/branch/main/install.sh)) that uses NVM (Node Version Manager) to install the latest LTS version of Node if needed, and subsequently installs Wolfram|Beta globally to the command line.

For Windows installations, it is recommended to install Node with [NVM for Windows](https://github.com/coreybutler/nvm-windows), as the above script will not work. It is worth noting, however, that [NVM for Mac / Linux](https://github.com/nvm-sh/nvm) is a completely different project.

### From Source
Make sure you have `yarn` or `npm`. Project was built in Node v18 but should be back compatible.

Clone the repository:
   ```bash
   git clone https://codeberg.org/woflydev/WolframBeta.git
   cd WolframBeta
   yarn
   ```

### From NPM (Recommended for Windows)
Ensure you have Node installed, either via Standalone or NVM. Simply add globally via the package manager of your choice:
```bash
npm i wolfram-beta -g
```
```bash
yarn global add wolfram-beta
```

## Usage
Run the script with the desired options to query Wolfram|Alpha and display the results in the terminal.

```bash
yarn start [options]
```

or

```bash
wolfram-beta [options]
```

Alternatively, you can quickly launch specific flags (repo clone only).

```bash
yarn ask [options] # launches with -i
yarn dev [options] # launches with -d
yarn help [options] # launches with -h and exits
yarn test [options] # launches with -t and uses default input
yarn version # launches with -v
```

Available options:

- `-u, --update`: Update Wolfram|Beta to the latest version. Overrides all other options and exits.
- `-v, --version`: Show the current version of Wolfram|Beta. Overrides all other options except `-u` and `--update`, then exits.
- `-d, --dev`: Show technical information JSON for debugging.
- `-s, --source`: Show source information of the query.
- `-o, --open`: Open the generated HTML file in the default browser.
- `-t, --test`: Run the program in test mode, and use the default input.
- `-i, --input <question>`: Directly use the input provided from the command line. Will be overriden by `-t` and `--test`.
- `-w, --wait <timeout>`: Wait a specified amount of time after forming a connection before grabbing information, useful for slow internet connections.
- `-h, --help`: Display available options on launch, then exit.

### Example
```bash
> node wolfram-beta
$ What do you want to calculate or know about?
> population of spain
$
$ processing...
$ Query Result Information:
$
$ Input interpretation
$ Spain | population
$
$ Result
$ 47.5 million people (world rank: 32nd) (2023 estimate)
$ Image(s) Found!
$ https://www6b3.wolframalpha.com/Calculate/MSP/MSP13381208dc8g4d56degb0000627c66d4a46301hf?MSPStoreType=image/gif&s=4
$
$ ...(there's more)
```

The best part? It formats it all nicely with colours and headings, to make it easier to read. It also outputs all of the parsed content (including images) to an HTML file in the same directory, which would be better for viewing multiple plots in one go. The HTML file is **always generated** but you can pass in the `-o` flag to open it automatically on completion.

## How It Works

The package leverages Puppeteer to launch a headless browser instance, essentially 'piggybacking' off of the wonderful work of contributors over at [Wolfree|Alpha](https://codeberg.org/wolfree) to provide step-by-step solutions. The resulting JSON is scraped with CSS selectors and contains answer results which are parsed and sorted.

## Contributing

Contributions are more than welcome! The codebase is a mess though (since I wrote it) so be warned. If you encounter any issues or have suggestions for improvements, please open an issue or submit a pull request.

1. Fork the repository.
2. Create a new branch for your feature: `git checkout -b feature-name`
3. Make your changes and commit them: `git commit -m 'some feature or fix'`
4. Push the changes to your fork: `git push origin feature-name`
5. Submit a pull request.

## License

Do whatever you want with it under GPL-v3.


![photo2](docs/photo2.png)