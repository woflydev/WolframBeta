#!/usr/bin/env node

const puppeteer = require('puppeteer');
const prompt = require('prompt-sync')();
const commander = require('commander');
const termkit = require('terminal-kit');
const _ = require('underscore');

async function main() {
	const opener = await import('open');

	function isEmpty(variable) {
		if (variable === undefined || variable === null) {
		return true;
		}
		if (typeof variable === 'string' && variable.trim() === '') {
		return true;
		}
		if (Array.isArray(variable) && variable.length === 0) {
		return true;
		}
		if (typeof variable === 'object' && Object.keys(variable).length === 0) {
		return true;
		}
		return false;
	}

	commander
	.name("wolfram-beta")
	.description("A terminal client with image support for Wolfram|Alpha Pro, built on top of the Wolfree framework.")

	commander
	.option('-i, --input <question>', 'Directly use the input provided from the command line. Will be overriden by "-t" and "--test".')
	.option('-w, --wait <timeout>', 'Wait for the specified amount of time (seconds) before scraping the page. Useful for slow internet connections.')
	.option('-d, --dev', 'Show technical information JSON - useful for debugging or granular details.')
	.option('-s, --source', 'Show source information.')
	.option('-t, --test', 'Test the script with the default input. Will override any input provided, as well as "-i" and "--input".')
	.option('-o, --open', 'Open the generated HTML file in the default browser.')
	.option('-v, --version', 'Show the current version of Wolfram|Beta. Overrides all options except "-u" and exits.')
	.option('-u, --update', 'Update Wolfram|Beta to the latest version. Overrides all other options and exits.')
	.parse(process.argv);
	const options = commander.opts();

	(async () => {
		const term = termkit.terminal;

		if (options.update) {
			/*term.magenta.italic.noFormat(`Wolfram|Beta is updating to the newest version...\n`);
			var updateOut = exec(`npm update -g ${packageJSON.name}`, (err, stdout, stderr) => {
				if (err) { console.log(`A problem occured while updating: ${err}\n`); return; }
				//term.yellow.noFormat(stdout);
			});
			
			await new Promise( (resolve) => {
				term.green.bold.noFormat(`Congrats! Wolfram|Beta was updated!\n`);
				updateOut.on('close', resolve); // wait for the process to finish
			}); */
			var packageJSON = require('./package.json');
			term.blue.bold.noFormat("Wolfram|Beta "); term.magenta.italic.noFormat("should be updated through either "); term.blue.bold.noFormat("npm"); term.magenta.italic.noFormat(" or "); term.blue.bold.noFormat("yarn"); term.magenta.italic.noFormat(".\n");
			term.magenta.italic.noFormat("You can do this by running "); term.green.bold.noFormat(`npm update -g ${packageJSON.name}`); term.magenta.italic.noFormat(" or "); term.green.bold.noFormat(`yarn global upgrade ${packageJSON.name}`); term.magenta.italic.noFormat(".\n");
			process.exit(0);
		}

		if (options.version) {
			var packageJSON = require('./package.json');
			term.green.bold.noFormat(`Wolfram|Beta v${packageJSON.version}\n`);
			term.magenta.italic.noFormat(`Created by ${packageJSON.author}\n`);
			term.magenta.italic.noFormat(`Licensed under the ${packageJSON.license} license.\n`);
			term.magenta.italic.noFormat(`View the source code at ${packageJSON.repository.url}\n`);
			term.blue.bold.noFormat(`Update with `); term.yellow.bold.noFormat(`${packageJSON.name} -u`); term.blue.bold.noFormat(` or `); term.yellow.bold.noFormat(`${packageJSON.name} --update`); term.blue.bold.noFormat(`.\n`);
			process.exit(0);
		}

		// noFormat should be used, 
		term.cyan.noFormat('Initializing...\n\n');
		await new Promise(resolve => setTimeout(resolve, 1)); // Add a delay to show the initializing text
		term.eraseLineAfter();
	
		// shows activated options (if any)
		for (const option in options) {
			term.green.noFormat(`  => ${option}: ${options[option] ? 'enabled' : 'disabled'}\n`);
		}

		const browser = await puppeteer.launch({ headless: "new" });
		const page = await browser.newPage();
	
		const defaultInput = "x^2 + 2x + 1 = 0";
		term.yellow.noFormat(`\nWelcome to `); term.green.noFormat(`Wolfram|Beta®!`); term.yellow.noFormat(` (probably just violated like 300 national laws lol)\n`)
		
		var input = "";
		if (!options.test) {
			if (!options.input) {
				term.yellow.noFormat(`If no input is provided, this default is used as an example: ${defaultInput}\n`);
				term.yellow.noFormat(`Use `); term.blue.bold.noFormat(`--help`); term.yellow.noFormat(` or `); term.bold.blue.noFormat(`-h`); term.yellow.noFormat(` when launching to see a list of arguments.\n\n`);
				input = prompt("What do you want to calculate or know about? ").trim(); // js just broke
			}

			else {
				term.yellow.noFormat(`Launched with command line input!\n\n`);
				input = options.input;
			}

			if (input.trim() === "") {
				term.yellow.noFormat('Using default input: ' + defaultInput + '\n\n');
				input = defaultInput;
			} else { term.green.noFormat('Using provided input: ' + input + '\n\n'); }
		}

		else {
			term.yellow.noFormat(`Launched with test mode enabled! Using default input: ${defaultInput}\n\n`);
			input = defaultInput;
		}

		const encodedInput = encodeURIComponent(input);
		
	term.magenta.italic.noFormat("Connecting to Wolfram|Alpha...\n")

	// navigate
	await page.goto('https://wolfreealpha.gitlab.io/input?i=' + encodedInput);
	
	term.magenta.italic.noFormat("Loading content...\n")
	
	var timeout = (options.wait) ? options.wait * 1000 : 8000;
	await new Promise(r => setTimeout(r, timeout)); // this can be ~5000 for headless mode, should be 10000 (10 sec) for windowed mode

	term.magenta.italic.noFormat("Grabbing content...\n")

	const selectors = ['.wolfree-pods', 'section', '._2QGO'];
	const scrapedContent = [];

	for (const selector of selectors) {
		const content = await page.evaluate(selector => {
		const elements = Array.from(document.querySelectorAll(selector));
		return elements.map(element => element.innerHTML);
		}, selector);

		scrapedContent.push(content);
	}

	await browser.close();

	term.magenta.italic.noFormat("Parsing content...\n\n")

	const jsonRegex = /<textarea name="technical-information">(.*?)<\/textarea>/s;

	let technicalInfoJsonString = null;
	for (const content of scrapedContent) {
		for (const item of content) {
		const match = item.match(jsonRegex);
		if (match) {
			technicalInfoJsonString = match[1];
			break;
		}
		}
		if (technicalInfoJsonString) {
		break;
		}
	}

	// only show technical information if --dev or -d flag is passed
	if (options.dev && technicalInfoJsonString) {
		console.log('DEBUGGING - Raw JSON String: \n' + technicalInfoJsonString);
	}

	try {
		const technicalInfoJson = JSON.parse(technicalInfoJsonString);
		const term = termkit.terminal;

		if (options.host && technicalInfoJson.document.location) {
			const locationInfo = technicalInfoJson.document.location;
			term.blue.underline.bold.noFormat('Source Information:\n\n');
			for (const key in locationInfo) {
			const value = isEmpty(locationInfo[key]) ? null : locationInfo[key];
			term.white.noFormat(`${key}: ${value}\n`);
			}
		}
		
		if (technicalInfoJson.response.queryresult) {
			const queryResult = technicalInfoJson.response.queryresult;
			term.blue.underline.bold.noFormat('\nQuery Result Information:\n\n');
			term.white.noFormat(`Success: ${queryResult.success}\n`);
			term.white.noFormat(`Error: ${queryResult.error}\n`);
			term.white.noFormat(`Query ID: ${queryResult.id}\n`);
			term.white.noFormat(`Related Queries: ${queryResult.related}\n`);
		}

		// Output details of the 'pods' array
		if (technicalInfoJson.response.queryresult.pods) {
		const pods = technicalInfoJson.response.queryresult.pods;
		term.bold.underline.cyan.noFormat('\nInformation about your query:\n\n');
		
		for (const pod of pods) {
			term.blue.bold(pod.title + '\n');
			
			if (pod.subpods && pod.subpods.length > 0) {
			var images = [];
			for (const subpod of pod.subpods) {
				var plaintext = subpod.plaintext || 'null';
				if (subpod.img && subpod.img.src) {
					if (plaintext === 'null') {
						images += _.unescape(subpod.img.src) + '\n';
					}
					else {
						term.white.noFormat(_.unescape(plaintext) + '\n\n');
						images += _.unescape(subpod.img.src)+ '\n';
					}
				}
			}

			term.magenta.noFormat("Image(s) Found!\n");
			term.white.noFormat(images + '\n')
			} else {
			term.white.noFormat('No subpods in query found.\n');
			}

			term('\n');
		}
		} else {
		term.red.noFormat('No answers found! Wolfram|Beta did not understand your request.\n');
		}
	} catch (error) {
		console.error('Error parsing technical information JSON:', error);
	}

	// generate html
	let html = `
		<!DOCTYPE html>
		<html>
		<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Results</title>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
		<style>
			body {
			background-color: #f8f9fa;
			color: #333;
			font-family: Arial, sans-serif;
			display: flex;
			justify-content: center;
			align-items: center;
			min-height: 100vh;
			margin: 0;
			}
			.container {
			background-color: #ffffff;
			padding: 20px;
			border-radius: 10px;
			box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
			}
			h2 {
			color: #007bff;
			font-size: 1.5rem;
			margin-bottom: 1rem;
			}
			.list-group-item {
			background-color: #ffffff;
			border: none;
			}
			.list-group-item:first-child {
			border-top-left-radius: 10px;
			border-top-right-radius: 10px;
			}
			.list-group-item:last-child {
			border-bottom-left-radius: 10px;
			border-bottom-right-radius: 10px;
			}
		</style>
		</head>
		<body>
		<div class="container">
	`;

	for (let i = 0; i < selectors.length; i++) {
		html += `
		<div class="mb-4">
			<h2>Output: ${selectors[i]}</h2>
			<ul class="list-group">
		`;
		
		for (const content of scrapedContent[i]) {
		html += `
			<li class="list-group-item">${content}</li>
		`;
		}

		html += `
			</ul>
		</div>
		`;
	}

	html += `
		</div>
		</body>
		</html>
	`;

	const fs = require('fs');
	fs.writeFileSync('answer.html', html);

	// open html file in default browser
	if (options.open) {
		await opener.default('answer.html');
	}

	term.magenta.italic.noFormat("Done!\n\n")
	})();
}

main();