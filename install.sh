echo "This will install the latest Node LTS on your Linux or Mac machine through NVM (Node Version Manager)."
echo "After installation of NVM and Node, this script will run 'npm install -g wolfram-beta' to install Wolfram|Beta."

read -n 1 -s -r -p "Press any key to continue..."

# Check if NVM is installed
if ! command -v nvm &> /dev/null
then
    echo "NVM is not installed. Installing NVM..."
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.5/install.sh | bash
    echo "NVM installed."
else
    echo "NVM is installed."
fi

# Check if Node is installed
if ! command -v node &> /dev/null
then
    echo "Node is not installed. Installing Node..."
    nvm install --lts
    echo "Node installed."
else
    echo "Node is installed already!"
fi

# Check if Wolfram|Beta is installed
if ! command -v wolfram-beta &> /dev/null
then
    echo "Installing Wolfram|Beta..."
    npm install -g wolfram-beta
    echo "Wolfram|Beta has been installed! Have fun!"
else
    echo "Wolfram|Beta is installed already."
fi

echo "Installation complete."